# what-is-on-sketchgraph

Just an exploration of Sketchgraph's data. Take a look at [the official
repository](https://github.com/PrincetonLIPS/SketchGraphs ).

## TLDR

Read the data like so:

1. Download either the filtered or unfiltered `.npy` files (see below or the
repo for links).

2. `from from sketchgraphs.data import flat_array`.

3. `seq_data = flat_array.load_dictionary_flat(npy_file)`

So that `seq_data` is a `list` with
`NodeOp`s and `EdgeOp`s (see below).

## Things not in the repo

1. The data. Elsewhere there is

	- raw JSON data ([look here](https://github.com/PrincetonLIPS/SketchGraphs )),

	- custom-formated 15GB binary data ([look here](https://sketchgraphs.cs.princeton.edu/sequence/sg_all.npy )) and

	- filtered sequence data (split into
	[training](https://sketchgraphs.cs.princeton.edu/sequence/sg_t16_train.npy ),
	[testing](https://sketchgraphs.cs.princeton.edu/sequence/sg_t16_test.npy ),
	and [validation](https://sketchgraphs.cs.princeton.edu/sequence/sg_t16_validation.npy )
	sets).

**Reading sequence data is easy** (see the "As a sequence" section).

The difference between the unfiltered and filtered data is that the latter filters "out sketches that are too large or too small, and only includes a simplified set of entities and constraints". Also the size (15GB vs less than 7GB total).

## Things in the repo

1. High level data representations (as a
[sequence](https://github.com/PrincetonLIPS/SketchGraphs/blob/master/sketchgraphs/data/sequence.py )
and as a
["sketch"](https://github.com/PrincetonLIPS/SketchGraphs/blob/master/sketchgraphs/data/sketch.py )
, see below).

2. Visualization tools (see below, implemented
[here](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs/data/_plotting.py#L61 )).

3. Models (implemented on
[Python](https://github.com/PrincetonLIPS/SketchGraphs/blob/master/sketchgraphs_models/graph/model/__init__.py )
with C++ extensions available).
Note that these models [handle constraints which relate at most two
entities](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs_models/__init__.py#L6).

## Structure of the data

From [the paper](https://arxiv.org/pdf/2007.08506.pdf ):

Sketches are "*composed of geometric primitives (line segments, circles, etc.) with associated parameters(coordinates, radius, etc.). Primitives and parameters interact via imposed constraints (e.g., equality,symmetry, perpendicularity, coincidence) determining their final configuration*".

The dataset includes the order in
which the primitives and the
constraints are added (but see below for
details). **This is the basis of the
sequence representation**.

Primitives and constraints induce a
graph where they are nodes and
edges respectively. **This is the
basis of the graph representation**.

Primitives and contraints are defined either as [`EntityType`](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs/data/_entity.py#L13 ) or
[`ConstraintType`](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs/data/_constraint.py#L12 ), which
are put either into [`NodeOp`](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs/data/sequence.py#L19)s and
[`EdgeOp`](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs/data/sequence.py#L28)s in
the sequence representation, or into `dict`s in the graph representation.

### Entities

This is the list of entities:

- Point
- Line
- Circle
- Ellipse
- Spline
- Conic
- Arc
- External
- Stop
- Unknown

#### Subprimitives

The authors note that "*Often, constraints are applied to a specific point on a primitive. For example, two endpoints from different line segments may be constrained to be a certain distance apart. In order to unambiguously represent these constraints, we include these sub-primitives as separate nodes in the constraint graph.*"

### Constraints

This is the list of constraints:

- Coincident
- Projected
- Mirror
- Distance
- Horizontal
- Parallel
- Vertical
- Tangent
- Length
- Perpendicular
- Midpoint
- Equal
- Diameter
- Offset
- Radius
- Concentric
- Fix
- Angle
- Circular_Pattern
- Pierce
- Linear_Pattern
- Centerline_Dimension
- Intersected
- Silhoutted
- Quadrant
- Normal
- Minor_Diameter
- Major_Diameter
- Rho
- Unknown
- Subnode

### As a sequence

This is implemented [here](https://github.com/PrincetonLIPS/SketchGraphs/blob/master/sketchgraphs/data/sequence.py )

This is a python `list` "consisting of `NodeOp` representing entities, and `EdgeOp` representing constraints".

`NodeOp`s look like this:

```
NodeOp(label=<EntityType.Line: 1>, parameters={'isConstruction': False, 'dirX': 1.0, 'dirY': 0.0, 'pntX': -0.0011842120438814163, 'pntY': 0.0, 'startParam': -0.01264495235582774, 'endParam': 0.02118421204388142})
```

```
NodeOp(label=<SubnodeType.SN_Start: 101>, parameters={})
```

`EdgeOp`s look like this:

```
EdgeOp(label=<ConstraintType.Vertical: 6>, references=(4,), parameters={})
```

```
EdgeOp(label=<ConstraintType.Coincident: 0>, references=(5, 3), parameters={})
```

The order in the list is described as follows:

- The nodes are ordered as they "were added to a sketch by the user".

- The edges are placed "immediately following the insertion of its member nodes".

- Onshape only records the relative
order among edges and nodes, which is
why the order does not necessarily
correspond to how the sketch was drawn (but
it is a good approximation).

To load the sequences in a file from the dataset and plot one of them
one may do as follows (also see [this
example from the repo](https://github.com/PrincetonLIPS/SketchGraphs/blob/master/demos/sketchgraphs_demo.ipynb )):


```.py
import sketchgraphs.data as datalib
from sketchgraphs.data import flat_array

filename = 'sequence_data/sg_t16_validation.npy'
seq_i_to_plot = 0

# Decode binary file
seq_data = flat_array.load_dictionary_flat(filename)

# Read sequence
seq_to_plot = seq_data[seq_to_plot]

# Convert to sketch and plot
sketch = datalib.sketch_from_sequence(seq)
datalib.render_sketch(sketch);
```

### As a graph

This is implemented [here](https://github.com/PrincetonLIPS/SketchGraphs/blob/master/sketchgraphs/data/sketch.py ).

The graph is represented as a pair of
`dicts`, meant to represent data "as obtained from Onshape in a structured
    and faithful manner".

The representation provides structure
to JSON files by parsing into
[`Entity`](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs/data/_entity.py#L97 ) and [`Constraint`](https://github.com/PrincetonLIPS/SketchGraphs/blob/2b0617443a9f16a84df6d668df7e18f9ab80a9c7/sketchgraphs/data/_constraint.py#L429 ) class instances.

The `entities` and `constraints` `dicts` map IDs to `Entity`s and `Constraint`s respectively.
